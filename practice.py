import json

#generator function to find first solution and return it immediately

def allocate(data, ans = None, resources_usage= None):
    '''
         Intervention schedule allocation based upon backtracking
    '''

    if ans is None:
        ans = {}

    if resources_usage is None:
        resources_usage =  {}
        count = 0
        for resources_name, value in data["Resources"].items():
            count +=1
        
        for i in range(1,count+1):
            resources_usage["Ressources_"+str(i)] = [0]*(data["T"])


    if len(ans) == len(data["Interventions"]):
        # handled all the interventions
        yield ans  #return a generator
        #end here when all interventions have been assigned

    # Check next intervention
    for intervention_name, intervention in data["Interventions"].items():
        #intervention is not yet assign 
        if intervention_name not in ans:
            # will add items not in ans
            # last day intervention can be run
            last_day = data["T"]  
            
            for day in range(1, last_day+1):

                str_day = str(day)
                #verify the starting day with the tmax for each intervention
                if day <= int(intervention["tmax"]):
                    # number of days it takes to run
                    delta = intervention["Delta"][day-1]  
                    # must finish within at least last day
                    if day + delta <= last_day:  
                            #before assigning the intervention for this day check to see if there is resource to use for that day or not  
                            # i.e we want to verify that this starting day has the necessary resources to run or not   
                        missing_resources = False
                        for k, v in intervention["workload"].items():
                            if not str_day in v:
                                missing_resources = True
                                break
                                    #raise Exception(f'{intervention_name} resources {k} missing day {str_day}')
                        if missing_resources:
                            continue # no resources to use for this day 
                            
                        #verify all kind of Ressources_X involve with this intervention
                        for resources_name, start_day in intervention["workload"].items():
                            for i in range(0,int(delta)+1):
                                resources_usage[resources_name][day+i-1]+= start_day[str(day + i)][str_day]
                            
                        #verify resources_usage with the original list of Ressources_X that contain max value at the beginning of the json file    
                        for resources_name, index in resources_usage.items():
                            for i in range(0,T+1):
                               if resources_usage[resources_name][i] > data["Resources"][resources_name]["max"][i]:
                                   break

                        # Check on Exclusion
                        else:

                            winter = data["Seasons"]['winter']
                            summer = data["Seasons"]['summer']
                            isa = data["Seasons"]['is']
                            if str_day in winter:
                                season = "winter"
                            elif str_day in summer:
                                season = "summer"
                            elif str_day in isa:
                                season = "is"
                            else:
                                season = ""
                                
                            exclusions = data["Exclusions"]
                            bExclude = False
                            for k, v_lst in exclusions.items():
                                if season in v_lst and intervention_name in v_lst:
                                    for intervention_k, ans_response in ans.items():
                                        if intervention_k in v_lst:
                                            ans_day = ans_response["Day"]
                                            ans_delta = ans_response["Delta"]
                                            if day == ans_day:
                                                bExclude = True
                                                break
                                    if bExclude:
                                        break
                            if not bExclude:
                                # Resources used
                                response = {"Day": day, "Delta": intervention["Delta"][day-1]}
                                resources = {k:v[str_day][str_day] for k, v in intervention["workload"].items()}
                                response['Resources'] = resources
                                ans[intervention_name] = response
                                
                                yield from allocate(data, ans, resources_usage)


with open('sujet/A_08.json', 'r') as f:
    data = json.load(f)                          
       
# Get first solution using from allocate generator                 
answer = next(allocate(data), None)   #get the next solution out of the function allocate and go into suspended mode 


if answer:
    for name, intervention in answer.items():
        print(f'Intervention {name}')
        print(f'\tStart day {intervention["Day"]}, Duration: {intervention["Delta"]}')
        resources = intervention["Resources"]
        print()