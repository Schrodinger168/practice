from math import sqrt
import numba
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.metrics import matthews_corrcoef


def merit_calculation_for_binary_dataset_using_matthews_coef(X, y):
    n_sample, n_feature = X.shape
    # feature-class correlation
    rcf_all = []
    for i in range(n_feature):
        coeff_c_f = abs(matthews_corrcoef(X[:, i], y))
        # print(coeff_c_f)
        rcf_all.append(coeff_c_f)
    rcf = np.mean(rcf_all)

    # feature-feature correlation
    rff_all = []
    count = 0
    for i in range(n_feature):
        for j in range(i + 1, n_feature):
            coeff_f_f = abs(matthews_corrcoef(X[:, i], X[:, j]))
            rff_all.append(coeff_f_f)
            count += 1
    rff = np.mean(rff_all)
    print(rcf, rff)


@numba.njit
def _fill_cm(m, c1, c2):
    m[:] = 0

    for a, b in zip(c1, c2):
        m[a, b] += 1


@numba.njit
def mcc(confusion_matrix):
    # https://stackoverflow.com/a/56875660/992687

    tp = confusion_matrix[0, 0]
    tn = confusion_matrix[1, 1]
    fp = confusion_matrix[1, 0]
    fn = confusion_matrix[0, 1]

    x = (tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)
    return ((tp * tn) - (fp * fn)) / sqrt(x + 1e-6)


@numba.njit(parallel=True)
def calculate_mcc_numba_X_y_parallel(X, y):
    rows, columns = X.shape

    num_cpus = numba.get_num_threads()

    # for each thread, allocate array for confusion matrix
    cms = []
    for i in range(num_cpus):
        cms.append(np.empty((2, 2), dtype="float32"))

    out = np.zeros(shape=(columns, columns), dtype="float32")

    # make indexes for each thread
    thread_column_idxs = np.array_split(np.arange(columns), num_cpus)

    out = np.empty(shape=columns, dtype="float32")
    for thread_idx in numba.prange(num_cpus):
        for i in thread_column_idxs[thread_idx]:
            _fill_cm(cms[thread_idx], X[:, i], y)
            out[i] = abs(mcc(cms[thread_idx]))
    return sum(out) / len(out)


@numba.njit(parallel=True)
def calculate_mcc_numba_X_parallel(X):
    rows, columns = X.shape

    num_cpus = numba.get_num_threads()

    # for each thread, allocate array for confusion matrix
    cms = []
    for i in range(num_cpus):
        cms.append(np.empty((2, 2), dtype="float32"))

    out = np.empty(shape=(columns, columns), dtype="float32")

    # make indexes for each thread
    thread_column_idxs = np.array_split(np.arange(columns), num_cpus)

    for i in range(columns):
        c1 = X[:, i]

        for thread_idx in numba.prange(num_cpus):
            for j in thread_column_idxs[thread_idx]:
                if j < i + 1:
                    continue

                c2 = X[:, j]
                _fill_cm(cms[thread_idx], c1, c2)
                out[i, j] = abs(mcc(cms[thread_idx]))

    out2, cnt = 0.0, 0
    for i in range(columns):
        for j in range(i + 1, columns):
            out2 += out[i, j]
            cnt += 1

    return out2 / cnt


def calculate_mcc(X, y):
    rcf, rff = calculate_mcc_numba_X_y_parallel(X, y), calculate_mcc_numba_X_parallel(X)
    print(rcf, rff)


dataset = pd.read_csv("../../dataframeCP4IM/dorothea.csv")
# matrix X
X = dataset.iloc[:, :-1].values
# y label
y = dataset.iloc[:, -1].values

list_idx = [96916, 97773, 97871, 97935, 98484, 98515, 98610, 98656, 98823, 98850, 98909, 98943, 99030, 99065, 99067,
            99074, 99136, 99190, 99201, 99213, 99215, 99226, 99238, 99241, 99259, 99279, 99285, 99317, 99324, 99358,
            99377, 99404, 99437, 99441, 99443, 99456, 99458, 99484, 99495, 99497, 99505, 99519, 99531, 99535, 99542,
            99545, 99554, 99555, 99584, 99586, 99594, 99612, 99617, 99618, 99619, 99624, 99643, 99645, 99673, 99675,
            99676, 99681, 99694, 99706, 99709, 99711, 99712, 99723, 99727, 99731, 99735, 99744, 99747, 99748, 99751,
            99752, 99753, 99754, 99756, 99758, 99767, 99768, 99780, 99783, 99785, 99786, 99790, 99793, 99794, 99795,
            99797, 99798, 99800, 99806, 99809, 99811, 99814, 99826, 99827, 99828, 99831, 99833, 99837, 99842, 99844,
            99849, 99850, 99855, 99858, 99865, 99868, 99874, 99877, 99886, 99889, 99893, 99894, 99897, 99900, 99906,
            99909, 99916, 99918, 99920, 99922, 99927, 99929, 99937, 99938, 99941, 99944, 99945, 99948, 99950,
            99951, 99952, 99955, 99957, 99958, 99960, 99963, 99964, 99966, 99972, 99978, 99979, 99982, 99984,
            99985, 99986, 99987, 99989, 99991, 99993, 99995, 99998, 99999]

X_subset = X[:, list_idx]
calculate_mcc(X=X_subset, y=y)  # numba
merit_calculation_for_binary_dataset_using_matthews_coef(X=X_subset, y=y)  # benchmark
