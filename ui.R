library(shiny)
library(shinyWidgets)
library(shinydashboard)
library(shinythemes)
library(leaflet)
library(magrittr)
library(rvest)
library(readxl)
library(dplyr)
library(maps)
library(ggplot2)
library(reshape2)
library(plotly)

# import data

cv_global <- read.csv("coronavirus_global.csv")


bootstrapPage(
  tags$head(includeHTML("gtag.html")),
  navbarPage(theme = shinytheme("flatly"), collapsible = TRUE,
             "VISUALISATION LE COVID-19", id="nav",
             
             tabPanel("GLOBAL",
                       sidebarPanel(
                         
                           span(tags$i(h6("Permet de visualiser la revolution du Covid-19 dans le monde dans la période de 4 mois")), style="color:#045a8d"),
                           
                           selectInput("condition","Choisir observation:",
                                       choices = c("Cas","Nouveaux cas","Décès","Nouveaux décès")),
                           
                           selectInput("display","Type de visualisation: ",
                                       choices = c("Histogramme","Camembert"))
                                   )   
                           
                        ),
    
             tabPanel("COVID-19 mapper",
                      div(class="outer",
                          tags$head(includeCSS("styles.css")),
                          leafletOutput("mymap", width="100%", height="100%")
                      )
                      
             ),
             mainPanel(
               plotOutput("image")
             )
  )
             
  )          
